package com.shanzhu.docker.shanzhudocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShanzhuDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShanzhuDockerApplication.class, args);
	}

}
