package com.shanzhu.docker.shanzhudocker.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/demo")
@RestController
public class DockerController {

    @RequestMapping("test")
    public String test() {

        return "docker部署springboot---整合docker:build到maven";
    }

}
